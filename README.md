# Command line tool: gitlab-tools

This is a command line tool for bulk transactions against a GitLab
server.

## Features

1. Add a list of reporters to a group.
2. Add a group as a member to all projects with a given name.
3. Print details of projects with a given name.
4. Submit specific files to MOSS for plagiarism detection.

## Installation

### Install stack
    
If you have `stack` installed, skip this bit.
    
__Note:__ with the instruction below about installing `stack`, only
use the https://get.haskellstack.org/ script if you have `sudo` or
root access to your machine. If not, read further down the stack
installation page for installing `stack` in userland.

Install the `stack` tool using these
[instructions](https://docs.haskellstack.org/en/stable/install_and_upgrade/).

### Install gitlab-tools

Clone this repository:

    git clone https://gitlab.com/robstewart57/gitlab-tools.git
    
Then compile and install it:

    cd gitlab-tools
    stack install
    
Make sure that `stack` installs the `gitlab-tools` executable file
into a directory which is included in your `$PATH`. On Linux, stack
installs to `~/.local/bin/`, so make sure that this directory is part
of `$PATH`.

## Usage

### Create an access token in GitLab

Go to your profile on the GitLab server you are using, e.g.

    https://<gitlab.example.com>/profile

Click _Access Tokens_ on the left.

Click _Create personal access token_.

Keep a record of the token, it is needed by the `--token`
`gitlab-tools` flag.

This token will grant you access to any GitLab data that you are able
to see on the GitLab web interface. Think of it as just another way to
viewing GitLab data, with the same access level if browsing the GitLab
web interface.

### Using gitlab-tools

```
$ gitlab-tools --help
Program to execute bulk GitLab actions

Usage: gitlab-tools --host host [--token token] [--userFilename userFilename]
                    [--group group] [--project project]
                    [--add-reporters-to-group] [--share-projects-with-group]
                    [--registered] [--plagiarism] [--mossUser mossUser]
                    [--mossLang mossLang]
                    [--plagiarismProject plagiarismProject]
                    [--plagiarismFiles plagiarismFiles] [--parent parent]
                    [--projectLinks]
  Executes actions against a GitLab server

Available options:
  --host host              URL of the GitLab server
  --token token            GitLab access token
  --userFilename userFilename
                           name of a CSV file with user IDs
  --group group            name of a group
  --project project        name of a project
  --add-reporters-to-group Add users as reporters to a group
  --share-projects-with-group
                           Share all projects with a given name to the specific
                           group
  --registered             prints 'yes' or 'no' depending on whether a user
                           exists on the GitLab server
  --plagiarism             use MOSS to detect plagiarism
  --mossUser mossUser      MOSS user ID
  --mossLang mossLang      languge to pass to MOSS
  --plagiarismProject plagiarismProject
                           project to detect plagiarism in
  --plagiarismFiles plagiarismFiles
                           comma separated list of plagiarism files
  --parent parent          parent username of skeleton code
  --projectLinks           get the URL links for searched for projects
  -h,--help                Show this help text
```

## Examples

### Adding users to a group

```
gitlab-tools \
    --host <GitLab url> --token <token> \
    --filename <filename>.csv \
    --add-reporters-to-group \
    --group <group name>
```

This command reads usernames from a comma separated file, and if they
are registered on the GitLab server it adds them to the `<group name>`
group with the _Reporter_ role.

### Getting details of projects

```
gitlab-tools \
    --host <GitLab url> --token <token> \
    --projectLinks \
    --project <project name>
```

This finds all projects named `project_name`. For each project found,
it will print:

* Continuous Integration result for the most recent commit.
* How many members the project has.
* The visibility of the project (private, internal or public).
* How many commits the project has.

### Getting details of projects for specific users

```
gitlab-tools \
    --host <GitLab url> --token <token> \
    --projectLinks \
    --project <project name> \
    --userFilename <CSV file path>
```

This attempts to find, for all user IDs in the CSV file, a project
named `project_name`. If the user does not have a project with that
name, this will be reported. When the user _does_ have a project with
name `project_name`, it will print:

* Continuous Integration result for the most recent commit.
* How many members the project has.
* The visibility of the project (private, internal or public).
* How many commits the project has.

### Adding a group to projects

```
gitlab-tools \
    --host <GitLab url> --token <token> \
    --share-projects-with-group --project <project name> \
    --group <group name>
```

This commands finds all projects called `<project name>` then adds the
`<group name>` group to all of those projects as a member with the
Reporter role.

### Check for code plagiarism

This is useful in the context assessing code in an education setting.

```
gitlab-tools \
  --host <GitLab url> --token <token> \
  --plagiarism \
  --mossUser <your MOSS user key> \
  --plagiarismFiles <files> \
  --parent <starter code user> \
  --userFilename <CSV user file path> \
  --plagiarismProject <project name> \
  --mossLang <programming language>
```
  
Where:

* `<your MOSS user key>` is generated by MOSS, see
  [here](http://theory.stanford.edu/~aiken/moss/)

* `<files>` is a string with comma separated file paths in the
  repository. These files are the ones that MOSS will check, i.e. the
  ones you think is at risk of plagiarism. E.g "src/Foo.hs,src/Bar.hs"

* `<starter code user>` is the user that has the "starter code" in a
  project in their namespace, from which other people have forked to
  work on their own versions.

* `<CSV file path>` is a full path for a CSV file containing comma
  separated user IDs of users to be checked for plagiarism.
  
* `<project name>` is the name of the project that each user will have
  forked to complete their work.
  
* `<programming language>` is to help MOSS detect plagiarism, by
  telling MOSS which programming language it is checking plagiarism
  for. Support languages are: C, CPP, Java, CSharp, Python,
  VisualBasic, Javascript, FORTRAN, ML, Haskell, Lisp, Scheme, Pascal,
  Modula2, Ada, Perl, TCL, Matlab, VHDL and Verilog. 

GitLab user IDs are anonymised before code is sent to
MOSS. `gitlab-tools` prints the mapping from anonymised IDs to real
GitLab IDs.
